#include "bootpack.h"
#include<stdio.h>

#define UNUSED 0
#define ALLOC  1
#define RUNNNING 2
struct ConsoleCtl consoleCtl;


void initConcoleCtl(void)
{
	int i = 0;
	consoleCtl.hasNow = -1;
	for (i = 0; i < MAXCONSOLESIZE; i++)
		consoleCtl.consolesStr[i].flag = UNUSED;
}

struct Console* getNewConsole(void)
{
	int i;
	for (i = 0; i < MAXCONSOLESIZE; i++)
		if (consoleCtl.consolesStr[i].flag == UNUSED)
		{
			consoleCtl.consolesStr[i].flag = ALLOC;
			break;
		}
	if (i == MAXCONSOLESIZE)return 0;//全部已经使用

	struct Console* ret = consoleCtl.consoles[++consoleCtl.hasNow] = &consoleCtl.consolesStr[i];//新申请的小黑窗
	
	ret->sheet = sheet_alloc(shtctl);
	ret->buf = (unsigned char*)memman_alloc_4k(memman, 256 * 165);
	sheet_setbuf(ret->sheet, ret->buf, 256, 165, -1); /*没有透明色*/
	make_window8(ret->buf, 256, 165, "console", 0);
	make_textbox8(ret->sheet, 8, 28, 240, 128, COL8_000000);
	ret->task = task_alloc();
	ret->task->tss.esp = memman_alloc_4k(memman, 64 * 1024) + 64 * 1024 - 12;
	ret->task->tss.eip = (int)&console_task;
	ret->task->tss.es = 1 * 8;
	ret->task->tss.cs = 2 * 8;
	ret->task->tss.ss = 1 * 8;
	ret->task->tss.ds = 1 * 8;
	ret->task->tss.fs = 1 * 8;
	ret->task->tss.gs = 1 * 8;
	*((int*)(ret->task->tss.esp + 4)) = (int)ret->sheet;
	*((int*)(ret->task->tss.esp + 8)) = memtotal;
	ret->sheet->task = ret->task;
	ret->sheet->flags |= 0x20; /*有光标*/
	ret->taskFifo = (int*)memman_alloc_4k(memman, 128 * 4);
	fifo32_init(&ret->task->fifo, 128, ret->taskFifo, ret->task);
	return ret;
}
void removeThisConsole(struct SHEET* sheet)
{
	int i = 0;
	struct Console* nowConsole;
	for (i = 0; i <= consoleCtl.hasNow; i++)
	{
		char s[100];
		sprintf(s, "%d %d", consoleCtl.consoles[i], consoleCtl.hasNow);
		putfonts8_asc_sht(sht_back, 20 + i * 80, 416, COL8_FFFFFF, COL8_008484, s, 10);
		if (consoleCtl.consoles[i]->sheet == sheet)
		{
			
			nowConsole = consoleCtl.consoles[i];
			break;
		}
	}
	if (i == consoleCtl.hasNow + 1)
	{
		char* s = "can't";
		putfonts8_asc_sht(sht_back, 10, 216, COL8_FFFFFF, COL8_008484, s, 10);
		return;
	}
	io_cli();
	nowConsole->task->cons->timer->fifo->task = 0;
	io_sti();
	/*nowConsole->task->cons->timer = 0;*/
	task_sleep(nowConsole->task);
	//ret->sheet = sheet_alloc(shtctl);
	sheet_free(nowConsole->sheet);
	//ret->taskFifo = (int*)memman_alloc_4k(memman, 128 * 4);
	memman_free_4k(memman, nowConsole->taskFifo, 128 * 4);
	//ret->task->tss.esp = memman_alloc_4k(memman, 64 * 1024) + 64 * 1024 - 12;
	memman_free_4k(memman, nowConsole->task->tss.esp, 64 * 1024);
	//ret->buf = (unsigned char*)memman_alloc_4k(memman, 256 * 165);
	memman_free_4k(memman, nowConsole->buf, 256 * 165);
	//ret->task = task_alloc();
	for (; i < consoleCtl.hasNow; i++)
		consoleCtl.consoles[i] = consoleCtl.consoles[i + 1];
	consoleCtl.hasNow--;
	nowConsole->flag = UNUSED;
}