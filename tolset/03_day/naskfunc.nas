; naskfunc
; TAB=4

[FORMAT "WCOFF"]				; 制作目标文件的模式	
[INSTRSET "i486p"]				; 使用到486为止的指令
[BITS 32]						; 3制作32位模式用的机器语言
[FILE "naskfunc.nas"]			; 文件名


; 制作目标文件的信息

		GLOBAL	_io_hlt			; 程序中包含的函数名
		GLOBAL	_write_mem8		;


; 以下是实际的函数

[SECTION .text]		; 目标文件中写了这些后再写程序

_io_hlt:	; void io_hlt(void);
		HLT
		RET

_write_mem8:    ;void write_mem8(int addr, int data);
		MOV ECX, [ESP + 4]
		MOV AL, [ESP + 8]
		MOV [ECX], AL
		RET
