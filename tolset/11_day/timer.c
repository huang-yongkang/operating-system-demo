#include "bootpack.h"

struct TimeUsrCtl timeUsrCtl;

#define UNUSED 0
#define ALLOC 1
#define USING 2

void initTimeUsrCtl()
{
	timeUsrCtl.end = -1;
	int i;
	for (i = 0; i < MAXTIMEUSER; i++)
		timeUsrCtl.timeUserData[i].isUsing = 0;
	timeUsrCtl.count = 0;
}

void freeTimer(struct TimeUser* now)
{
	now->isUsing = UNUSED;
	struct MEMMAN* memman = (struct MEMMAN*)MEMMAN_ADDR;
	memman_free(memman, now->fifo->buf, now->fifo->size);//释放缓冲区
	int i;
	for (; i < timeUsrCtl.end; i++)timeUsrCtl.timeUser[i] = timeUsrCtl.timeUser[i + 1];
	--timeUsrCtl.end;
}

void inthandler20(int* esp)
{
	io_out8(PIC0_OCW2, 0x60);
	timeUsrCtl.count += 10;

	int i = 0;
	for (; i <= timeUsrCtl.end; i++)
	{
		if(USING == timeUsrCtl.timeUser[i]->isUsing)
		{
			if (timeUsrCtl.timeUser[i]->count <= timeUsrCtl.count)
			{
				fifo8_put(timeUsrCtl.timeUser[i]->fifo, timeUsrCtl.timeUser[i]->uid);
				timeUsrCtl.timeUser[i]->isUsing = ALLOC;
			}
		}

	}

	return;
}

struct TimeUser* malloc_timer(int count)
{
	int i = 0, j = 0, flag = 0;
	for (i = 0; i < MAXTIMEUSER; i++)
		if (!timeUsrCtl.timeUserData[i].isUsing)
		{
			timeUsrCtl.timeUserData[i].isUsing = 1;
			timeUsrCtl.timeUserData[i].count = timeUsrCtl.count + count;
			flag = 1;
			break;
		}
	//这个尽量用指针
	if (!flag)return flag;
	int nowCount = timeUsrCtl.timeUserData[i].count;
	struct TimeUser* nowIn = &timeUsrCtl.timeUserData[i];
	for (i = 0; i <= timeUsrCtl.end; i++)
		if (timeUsrCtl.timeUser[i]->count > nowIn->count)break;
	//if (i > timeUsrCtl.end)--i;
	j = ++timeUsrCtl.end;
	for (; j > i; j--)timeUsrCtl.timeUser[j] = timeUsrCtl.timeUser[j - 1];
	timeUsrCtl.timeUser[j] = nowIn;
	nowIn->isUsing = USING;
	return nowIn;
}

void set_timer(struct TimeUser* timeUser, char uid, struct FIFO8* fifo)
{
	timeUser->isUsing = USING;
	timeUser->fifo = fifo;
	timeUser->uid = uid;
}

void reset_time(struct TimeUser* timeUser, int count)
{
	timeUser->count = timeUsrCtl.count + count;
	timeUser->isUsing = USING;
}

void init_pit(void)
{
	io_out8(PIT_CTRL, 0x34);
	//1000 = 1.19318KHz
	//11932 = 100Hz = 10ms = 0x2e9c
	//1ms = 0xa94
	io_out8(PIT_CNT0, 0x9c);
	io_out8(PIT_CNT0, 0x2e);
	//io_out8(PIT_CNT0, 0x12);
	return;
}

