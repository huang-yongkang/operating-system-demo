/* bootpack */

#include "bootpack.h"
#include <stdio.h>

int num;
struct KEYBUF keybuf;

void HariMain(void)
{
	struct BOOTINFO *binfo = (struct BOOTINFO *) ADR_BOOTINFO;
	char s[40], mcursor[256];
	int mx, my, i;

	init_gdtidt();
	init_pic();
	io_sti(); /* IDT/PIC的初始化已经完成，于是开放CPU的中断 */

	init_palette();
	init_screen8(binfo->vram, binfo->scrnx, binfo->scrny);
	mx = (binfo->scrnx - 16) / 2; /* 计算画面的中心坐标*/
	my = (binfo->scrny - 28 - 16) / 2;
	init_mouse_cursor8(mcursor, COL8_008484);
	putblock8_8(binfo->vram, binfo->scrnx, 16, 16, mx, my, mcursor, 16);
	sprintf(s, "(%d, %d)", mx, my);
	putfonts8_asc(binfo->vram, binfo->scrnx, 0, 0, COL8_FFFFFF, s);

	io_out8(PIC0_IMR, 0xf9); /* 开放PIC1和键盘中断(11111001) */
	io_out8(PIC1_IMR, 0xef); /* 开放鼠标中断(11101111) */

	for (;;)
	{
		io_cli();//关中断
		if (!keybuf.flag)io_stihlt();//休眠
		else
		{
			struct BOOTINFO* info = (struct BOOTINFO*)ADR_BOOTINFO;
			unsigned char now_s[10];
			unsigned now_data = keybuf.data;
			keybuf.flag = 0;
			io_sti();//开中断
			sprintf(now_s, "number:%d, %02X", num, now_data);
			boxfill8(info->vram, info->scrnx, COL8_008484, 5, 20, 320, 36);
			putfonts8_asc(info->vram, info->scrnx, 5, 20, COL8_FFFFFF, now_s);//........
		}
		
	}
}
