; naskfunc
; TAB=4

[INSTRSET "i486p"]				; 使用到486为止的指令
[BITS 32]						; 3制作32位模式用的机器语言
		MOV	EBX,	0x26f7e0
		MOV EAX,		1
		MOV	[EBX],		EAX
alios:
		MOV	EBX,		0x26f7f0
		MOV	EAX,		[EBX]
		INC	EAX
		MOV	[EBX],		EAX
		JMP	alios