#include "bootpack.h"

int languages[MAX_LANGUAGE_NUMBER];

void language_init(void)
{
	const static char* language_name[MAX_LANGUAGE_NUMBER] = {
	"hankaku",
	"chinese",
	"nihongo"
	};

	extern char hankaku[4096];
	struct MEMMAN* memman = (struct MEMMAN*)MEMMAN_ADDR;
	int* fat;
	struct FILEINFO* finfo;
	int i, siz;
	for (i = 0; i < MAX_LANGUAGE_NUMBER; i++)languages[i] = 0;//首先置零
	languages[0] = hankaku;

	/* 载入nihongo.fnt */
	fat = (int*)memman_alloc_4k(memman, 4 * 2880);
	file_readfat(fat, (unsigned char*)(ADR_DISKIMG + 0x000200));

	for (i = 1; i < LANGUAGE_NUMBER_NOW; i++)
	{
		char* now = 0;
		finfo = file_search(language_name[i], (struct FILEINFO*)(ADR_DISKIMG + 0x002600), 224);
		if (finfo != 0) {//找到了
			siz = finfo->size;
			now = file_loadfile2(finfo->clustno, &siz, fat);
		}
		else
		{
			//加上.fnt继续找
			char new_name[100];//转载新名字
			int j = 0;
			while (*(language_name[i] + j))new_name[j] = *(language_name[i] + j++);
			int cnt = 4;
			static const char* hind = ".fnt";
			while (cnt)new_name[j++] = hind[4 - cnt--];
			new_name[j] = 0;//加上.fnt后缀继续找

			//sprintf(s, "%d %d %d", languages[ASCLL], languages[CHINESE], languages[2]);
			putfonts8_asc_sht(sht_back, 10, 216, COL8_FFFFFF, COL8_008484, new_name, 100);

			finfo = file_search(new_name, (struct FILEINFO*)(ADR_DISKIMG + 0x002600), 224);
			if (finfo != 0) {//找到了
				siz = finfo->size;
				now = file_loadfile2(finfo->clustno, &siz, fat);
			}
		}
		languages[i] = now;//找到赋值该地址，未找到赋值为初值0，用处见graphic.c
	}
	memman_free_4k(memman, (int)fat, 4 * 2880);

}