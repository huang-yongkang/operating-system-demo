[FORMAT "WCOFF"]
[INSTRSET "i486p"]
[BITS 32]
[FILE "crack.nas"]

	GLOBAL _HariMain

[SECTION .text]

_HariMain:
	MOV	EAX, 0x999999
	JMP	EAX	
	MOV	AX,	1005 * 8
	MOV	DS, AX
	CMP DWORD	[DS:0X4], 'Hari' ;这里获得的就是代码段数据，使用cs寻址的原因是因为若改变的cs数据
								 ;会改变CPU指令寻址，导致执行到不可知区域

	JNE	fin

	MOV	ECX, [DS:0]
	MOV AX, 2005 * 8
	MOV DS, AX

crack_loop:
	ADD ECX, -1
	MOV	BYTE [DS:ECX], 123
	CMP ECX, 0
	JNE crack_loop

fin:
	MOV	EDX, 0X4
	INT 0x40